<?php
include "top.php";
?>

<div class="container mt-2">
    <div class="headline mb-3">
        <h2>Beyond Financial Black Swans: Rise of the Machines</h2>
    </div>
    <p><small>January 3, 2013 by Brian Tivnan</small></p>

    <div class="embed-responsive embed-responsive-16by9">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/C8VsmCOdTz0" style="border:none;"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<div class="container mt-4">
    <div class="headline mb-3">
        <h2>An Example of High Frequency Trader (HFT) Latency Arbitrage</h2>
    </div>
    <p><small>May 3, 2017 by MITRE</small></p>

    <div class="embed-responsive embed-responsive-16by9">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/1ltjnbBaFok?controls=0" style="border:none;"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

<?php
include "footer.php";
?>
