<?php
include "top.php";
include "zoom.php";
?>

<?php
$tickers = array();
foreach(glob("img/circle_plots/*") as $circle_plot) {
    $ticker = explode("_", basename($circle_plot))[0];
    array_push($tickers, $ticker);
}
$counts = array_count_values($tickers);
$tickers_set = array_unique($tickers);
$tickers = array();
foreach($tickers_set as $ticker) {
    if ($counts[$ticker] == 4)
        array_push($tickers, $ticker);
}
?>

<script type="text/javascript">
    let names = <?php echo json_encode($tickers) ?>;
</script>

<div class="container">
    <div class="headline mb-3 mt-2">
        <h2>Dislocation Segment Visualizer</h2>
    </div>
    <div class="alert alert-warning" role="alert" id="ie-warning" style="display: none">
        Warning! This page is best viewed in a modern browser such as Firefox, Chrome, or Edge.
        Due to performance issues with IE, only a subset of tickers are available when using this browser.
    </div>
    <p>Displays Dislocation Segments (DSs) observed in calender year 2016 for the selected ticker.</p>
    <p>DSs can arise when distinct information feeds display different quotes for the same security when viewed by the
        same observer at a definite, fixed location and time. In our work
        "Fragmentation and Inefficiencies in the U.S. Equity Markets: Evidence from the Dow 30" we consider DSs that
        arise between SIP and direct data feeds within the US National Market System.</p>

    <div class="form-group row">
        <div class="col-auto">
            <label class="sr-only" for="dropdown_tickers">Ticker</label>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown_tickers" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false" value="AAPL">
                    Ticker
                </button>
                <div id="eventMenu" class="dropdown-menu dropdown-container" aria-labelledby="dropdown_tickers">
                    <div class="px-4 py-2" id="inlineFormInput">
                        <input type="search" class="form-control" id="searchTicker" placeholder="AAPL" autofocus="autofocus">
                    </div>
                    <div id="menuItems" class="dropdown-content"></div>
                    <div id="empty" class="dropdown-header">Not in dataset</div>
                </div>
            </div>
        </div>
        <div class="col-auto">
            <label class="sr-only" for="dropdown_type">Time Type</label>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown_type"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" value="Real Time">
                    Real Time</button>
                <div class="dropdown-menu" aria-labelledby="dropdown_type" id="timeMenu">
                    <input class="dropdown-item" type="button" value="Real Time">
                    <input class="dropdown-item" type="button" value="Event Time">
                </div>
            </div>
        </div>
        <div class="col-auto">
            <label class="sr-only" for="dropdown_scale">Time Type</label>
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown_scale"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" value="Linear Scale">
                    Linear Scale</button>
                <div class="dropdown-menu" aria-labelledby="dropdown_type" id="scaleMenu">
                    <input class="dropdown-item" type="button" value="Linear Scale">
                    <input class="dropdown-item" type="button" value="Log Scale">
                </div>
            </div>
        </div>
    </div>

    <figure>
        <img id="lao" class="rounded d-block w-100" src="img/abstracts/AAPL.png" alt="Dislocation Segments">
    </figure>
</div>

<?php
include "footer.php";
?>

