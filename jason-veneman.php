<?php
include "top.php";
?>

<div class="jumbotron jumbotron-fluid rounded">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="img/profile/veneman.jpg" class="rounded mx-auto d-block w-100" alt="Jason Veneman">
            </div>

            <div class="col-md-6">
                <h1 class="display-4">Jason Veneman</h1>
                <p class="lead">Lead Artificial Intelligence Engineer at The MITRE Corporation
                </p>
                <p>Jason’s work is focused on the modeling and analysis of wicked problems from a complex systems
                    perspective. To help decision makers make sense of difficult socio-technical issues he has developed
                    and analyzed agent-based models that explore concerns ranging from equipment readiness and
                    counter-insurgency operations to financial markets and regional energy systems planning. Current
                    areas of interest include modeling the impact of cascading failures between interacting critical
                    infrastructures and measures of resilience in relation to policy decisions.</p>
            </div>
        </div>
    </div>
</div>


<?php
include "footer-min.php";
?>
