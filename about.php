<?php
include "top.php";
include "zoom.php";

$lat_arb = "<p>Diagram of two dislocation segments.  The inset plot shows the time series of best quotes that
 generate the dislocations. Where the time series diverge from the same value, a dislocation occurs. We have 
 deliberately not placed units on t, ∆p, and p to indicate that dislocations
  can occur in any market in which there are differing information feeds, not just in the NMS, though we do assume that
   these quantities are quantized. In the case of the NMS, we take t in units of μs and ∆p in units of $0.01.  
   The reader will note that this diagram represents one side(either bid or offer) of the book. The marker sizes in the
    inset time series subplot do not denote any particular aspect of the time series; the size of the D2 marker is larger
     than that of the D1 marker simply for visual distinction. The image is Figure 2 from
            \"Fragmentation and Inefficiencies in the U.S. Equity Markets: Evidence from the Dow 30\".</p>";
?>
<div class="row container mt-2">
    <figure class="col-lg-8 col-md-12">
        <img class="rounded d-block w-100 zoom" src="img/latency_arbitrage_opportunity.png" data-cap='<?php echo($lat_arb); ?>' alt="Dislocation segment example">
        <figcaption>A diagram of two dislocation segments. Please click the image for more details.
            The image is Figure 2 from
            "Fragmentation and Inefficiencies in the U.S. Equity Markets: Evidence from the Dow 30".
        </figcaption>
    </figure>
    <div class="col-md-4 d-none d-lg-block mt-2">
        <div class="headline mb-3"><h2>What is Computational Finance?</h2></div>
        <p>We extend from Blake LeBaron’s [2000] overview for a simple yet overarching definition of
            Computational Finance - the interdisciplinary application of numerical
            methods to measure and model the behavior of financial systems.</p>
    </div>
    <div class="d-lg-none col-md-4">
        <div class="headline mb-3"><h2>What is Computational Finance?</h2></div>
        <p>We extend from Blake LeBaron’s [2000] overview for a simple yet overarching definition of
            Computational Finance - the interdisciplinary application of numerical
            methods to measure and model the behavior of financial systems.</p>
    </div>
    <div class="col-md-4 mt-4">
        <div class="headline mb-3"><h2>About MITRE</h2></div>
        <p>
            <a href="https://www.mitre.org/">MITRE</a>'s mission-driven teams are dedicated to solving problems
            for a safer world.  Through its federally funded R&D centers and public-private partnerships,
            MITRE works across government to tackle challenges to the safety, stability, and well-being of
            both the nation and the world.
        </p>
    </div>
    <div class="col-md-4 mt-4">
        <div class="headline mb-3"><h2>UVM Partnership</h2></div>
        <p>One of these public-private partnerships is between MITRE and the University of Vermont to
            collaborate on innovative research at the Vermont Complex Systems Center.
            Computational Finance Lab, or CompFi, is yet another research initiative to stem from this
            partnership. </p>
    </div>
    <div class="col-md-4 mt-4">
        <div class="headline mb-3"><h2>Our Members</h2></div>
        <p>Founded by <a href="brian-tivnan.php">Brian Tivnan</a>, our lab has grown to include numerous researchers from
        MITRE, UVM, and beyond. You can learn more about our members <a href="team.php">here</a>. For those
            interested in joining our team or collaborating with us, please get in touch!</p>
    </div>
</div><!--/row-->

<?php
include "footer.php";
?>