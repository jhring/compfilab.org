<?php
include "top.php";
include "zoom.php";

$bc_img = '<p>The National Market System, colloquially known as the "stock market" is more complex than
                        many realize. The image is Figure 3 from
                        Fragmentation and Inefficiencies in the U.S. Equity Markets: Evidence from the Dow 30.</p>';
?>

<div class="row">
</div><!-- /row -->
<div class="row mt-2">
    <div class="col-md-6 mb-4">
        <div class="headline"><h2>Computational Finance Lab</h2></div>
        <p>The Computational Finance Lab is a joint venture of the University of Vermont and The MITRE Corporation.
            Its purpose is to study modern financial markets from a systems perspective using the tools of statistical
            physics, systems engineering, and data science. Its major research foci are empirical market microstructure
            and agent-based modeling of financial markets. You can learn more about our group <a href="about.php">here</a>.</p>

        <div class="headline"><h2>Latest News</h2></div>
        <p>We're in the Wall Street Journal! The article
            <a href="https://www.wsj.com/articles/brief-price-gaps-in-stocks-cost-investors-2-billion-a-year-11550152800">
                Brief Price Gaps in Stocks Cost Investors $2 Billion a Year</a> covers two of our working papers:
        "Fragmentation and Inefficiencies in the U.S. Equity Markets: Evidence from the Dow 30" and
            "Scaling of inefficiencies in the U.S. equity markets: Evidence from three market indices and more than 2900 securities"
        you can find more information about our work on the <a href="research.php">research</a> page.</p>

    </div>
    <div class="col-md-6 mb-4">
        <figure>
            <img class="rounded mx-auto d-block w-100 zoom" src="img/BC_network-500.png" data-big="img/BC_network.png" data-cap='<?php echo($bc_img); ?>'
            alt="The National Market System">
            <figcaption>
                <h3>A depiction of the National Market System</h3>
                <p><small>The National Market System, colloquially known as the "stock market" is more complex than
                        many realize (click image for more details). The image is Figure 3 from
                        "Fragmentation and Inefficiencies in the U.S. Equity Markets: Evidence from the Dow 30".</small></p>
            </figcaption>
        </figure>
    </div>
</div>

<?php
include "footer.php";
?>