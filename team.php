<?php
include "top.php";
?>

<div class="headline mb-3">
    <h2 class="my-4 text-center text-lg-left">Our Members</h2>
</div>

<div class="row text-center text-lg-left">

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <a href="chris-danforth.php">
            <img class="img-fluid figure-img img-thumbnail" src="img/profile/danforth-sm.jpg" alt="Danforth">
        </a>
        <figcaption>
            <a href="chris-danforth.php" class="unlink">
                <h3>Chris Danforth</h3>
            </a>
            <p><small>Associate Professor at UVM and Co-Director of Computational Story Lab</small></p>
        </figcaption>
    </figure>

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <img class="img-fluid figure-img img-thumbnail" src="img/profile/dewhurst-sm.jpg" alt="Dewhurst">
        <figcaption>
            <h3>David Dewhurst</h3>
            <p><small>Ph.D. Student at UVM and a Graduate Fellow at The MITRE Corporation</small></p>
        </figcaption>
    </figure>

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <img class="img-fluid figure-img img-thumbnail" src="img/profile/tyler-gray-sm.jpg" alt="Tyler Gray">
        <figcaption>
            <h3>Tyler Gray</h3>
            <p><small>Ph.D. Student at UVM</small></p>
        </figcaption>
    </figure>

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <a href="matt-koehler.php">
            <img class="img-fluid figure-img img-thumbnail" src="img/profile/koehler-sm.jpg" alt="Koehler">
        </a>
        <figcaption>
            <a href="matt-koehler.php" class="unlink">
                <h3>Matt Koehler</h3>
            </a>
            <p><small>Complexity Science Area Lead at The MITRE Corporation</small></p>
        </figcaption>
    </figure>

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <img class="img-fluid figure-img img-thumbnail" src="img/profile/mcmahon.jpg" alt="Mcmahon">
        <figcaption>
            <h3>Matt McMahon</h3>
            <p><small>Principal Modeling and Simulation Engineer at The MITRE Corporation</small></p>
        </figcaption>
    </figure>

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <img class="img-fluid figure-img img-thumbnail" src="img/profile/colin_headshot-sm.jpg" alt="Van Oort">
        <figcaption>
            <h3>Colin Van Oort</h3>
            <p><small>Ph.D. Student at UVM and a Graduate Fellow at The MITRE Corporation</small></p>
        </figcaption>
    </figure>

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <img class="img-fluid figure-img img-thumbnail" src="img/profile/ring-sm.jpg" alt="Ring">
        <figcaption>
            <h3>John H. Ring IV</h3>
            <p><small>Ph.D. Student at UVM and a Graduate Fellow at The MITRE Corporation</small></p>
        </figcaption>
    </figure>

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <a href="brendan-tivnan.php">
            <img class="img-fluid figure-img img-thumbnail" src="img/profile/brendan-sm.jpg" alt="Tivnan">
        </a>
        <figcaption>
            <a href="brendan-tivnan.php" class="unlink">
                <h3>Brendan Tivnan</h3>
            </a>
            <p><small>Research Intern at UVM's Complex Systems Center</small></p>
        </figcaption>
    </figure>

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <a href="brian-tivnan.php">
            <img class="img-fluid figure-img img-thumbnail" src="img/profile/btivnan-sm.jpg" alt="Tivnan">
        </a>
        <figcaption>
            <a href="brian-tivnan.php" class="unlink"><h3>Brian Tivnan</h3></a>
            <p><small>Chief Engineer at The MITRE Corporation and Adjunct Assistant Professor at UVM</small></p>
        </figcaption>
    </figure>

    <figure class="col-lg-3 col-md-4 col-xs-6">
        <a href="jason-veneman.php">
            <img class="img-fluid figure-img img-thumbnail" src="img/profile/veneman-sm.jpg" alt="Veneman">
        </a>
        <figcaption>
            <a href="jason-veneman.php" class="unlink"><h3>Jason Veneman</h3></a>
            <p><small>Lead Artificial Intelligence Engineer at The MITRE Corporation</small></p>
        </figcaption>
    </figure>

</div>
<?php
include "footer.php";
?>
