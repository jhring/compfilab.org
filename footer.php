<div class="copyright mt-4">
    <div class="headline mb-3">
        <h2>Affiliated Organizations</h2>
    </div>
    <div class="container">
        <div class="row d-flex flex-wrap align-items-center">
            <div class="col-4">
                <a href="https://www.mitre.org/">
                    <!-- Image to show on screens from small to extra large -->
                    <img class="d-none d-sm-block img-fluid" src="img/affilated/Mitre_Corporation_logo.png" alt="MITRE Logo">
                    <!-- Image to show on extra small screen (mobile portrait) -->
                    <img class="d-sm-none mx-auto img-fluid" src="img/affilated-sm/Mitre_Corporation_logo.png" alt="MITRE Logo">
                </a>
            </div>
            <div class="col-4">
                <a href="http://vermontcomplexsystems.org/">
                    <img class="d-none d-sm-block mx-auto img-fluid" src="img/affilated/roboctopus.png" alt="Vermont Complex Systems Center Logo">
                    <img class="d-sm-none mx-auto img-fluid" src="img/affilated-sm/roboctopus.png" alt="Vermont Complex Systems Center Logo">
                </a>
            </div>
            <div class="col-4">
                <a href="https://www.uvm.edu/">
                    <img class="d-none d-sm-block mx-auto img-fluid" src="img/affilated/vermontlogo.png" alt="UVM Logo">
                    <img class="d-sm-none mx-auto img-fluid" src="img/affilated-sm/vermontlogo.png" alt="UVM Logo">
                </a>
            </div>
        </div>
    </div>

    <div class="container">
        <p class="text-center"><small>Copyright ©2019 The MITRE Corporation and the University of Vermont. All rights reserved. Approved for public release. Distribution unlimited 18-03431-3.</small></p>
    </div>
    <div class="container">
        <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
            <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" />
        </a>This work is licensed under a
        <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">
            Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.
    </div>
</div><!--/copyright-->

</div><!--/container-->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>

<?php
if ($fname == "visualizer") {
    echo('<script src="js/dropdown.js"></script>');
}

$zoomed = array("index", "about", "research");
if (in_array($fname, $zoomed)) {
    echo('<script src="js/zoom.js"></script>');
}
?>

</body>
</html>