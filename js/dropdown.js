// inspired by https://stackoverflow.com/a/48220373

// for IE compatibility
if (window.document.documentMode) {
    // overwrite names for as IE11 can't handle the full list
    names = ['AAPL', 'AXP', 'BA', 'CAT', 'CSCO', 'CVX', 'DD', 'DIS', 'GE', 'GS', 'HD', 'IBM', 'INTC', 'IVV', 'IWN',
        'IWV', 'JNJ', 'JPM', 'KO', 'MCD', 'MMM', 'MRK', 'MSFT', 'NKE', 'PFE', 'PG', 'SPY', 'THRK', 'TRV', 'TWOK', 'UNH',
        'UTX', 'V', 'VOO', 'VTHR', 'VTWO', 'VZ', 'WMT', 'XOM'];

    document.getElementById("ie-warning").style.display = "block";
}

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}

//Find the input search box
let search = document.getElementById("searchTicker");

function buildDropDown(values) {
    let contents = [];
    for (let i = 0; i < values.length; i++) {
        contents.push('<input class="dropdown-item" type="button" value="' + values[i] + '" id="' + values[i] + '"/>')
    }
    $('#menuItems').append(contents.join(""));

    //Hide the row that shows no items were found
    $('#empty').css({display: 'none'})
}

function useText(newTicker) {
    if (names.indexOf(newTicker) >= 0) {
        $('#dropdown_tickers').text(newTicker);
        document.getElementById("dropdown_tickers").value = newTicker;
        $("#dropdown_tickers").dropdown('toggle');
        LAOUpdate();
    }

    if (names.indexOf(newTicker) >= 0) {
        $("#dropdown_tickers").dropdown('toggle');
    }
}

//Capture the event when user types into the search box
window.addEventListener('input', function () {
    filter(search.value.trim().toLowerCase())
});

$("#searchTicker").blur( function() {
    let newTicker = $(this)[0].value.toUpperCase();
    useText(newTicker);
});

search.addEventListener("keydown", function (e) {
    if (e.key === "Enter") {
        let newTicker = $(this)[0].value.toUpperCase().trim();
        useText(newTicker);
    }
});

//For every word entered by the user, check if the symbol starts with that word
//If it does show the symbol, else hide it
function filter(word) {
    let length = items.length;
    let hidden = 0;
    for (let i = 0; i < length; i++) {
        if (items[i].value.toLowerCase().startsWith(word)) {
            items[i].style.display = "block";
        }
        else {
            items[i].style.display = "none";
            hidden++
        }
    }

    //If all items are hidden, show the empty view
    if (hidden === length) {
        $('#empty').css({display: 'block'});
    }
    else {
        $('#empty').css({display: 'none'})
    }
    return false;
}

//If the user clicks on any item, set the title of the button as the text of the item
$('#menuItems').on('click', '.dropdown-item', function(){
    $('#dropdown_tickers').text($(this)[0].value);
    document.getElementById("dropdown_tickers").value = $(this)[0].value;
    $("#dropdown_tickers").dropdown('toggle');
    LAOUpdate()
});

$('#timeMenu').on('click', '.dropdown-item', function(){
    $('#dropdown_type').text($(this)[0].value);
    document.getElementById("dropdown_type").value = $(this)[0].value;
    $("#dropdown_type").dropdown('toggle');
    LAOUpdate()
});

$('#scaleMenu').on('click', '.dropdown-item', function(){
    $('#dropdown_scale').text($(this)[0].value);
    document.getElementById("dropdown_scale").value = $(this)[0].value;
    $("#dropdown_scale").dropdown('toggle');
    LAOUpdate()
});

function LAOUpdate() {
    let ticker = document.getElementById("dropdown_tickers").value;
    let type = document.getElementById("dropdown_type").value;
    let scale = document.getElementById("dropdown_scale").value;

    if (type === "Event Time") {
        type = "event"
    } else {
        type = "real"
    }

    if (scale === "Log Scale") {
        scale = "_log";
    } else {
        scale = "";
    }
    document.getElementById("lao").src = "img/circle_plots/" + ticker + "_545us_1c_1us_" + type + "_circle_graph" + scale + ".png";
}

buildDropDown(names);
let items = [];
for (let i = 0; i < names.length; i++) {
    items.push(document.getElementById(names[i]))
}
