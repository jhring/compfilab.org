$(function() {
    $('img').on('click', function() {
        if ($(this).hasClass('zoom') /*&& screen.width >= 576*/) {
            let source = $('.enlargeImageModalSource');
            if ($(this).attr('data-big') === undefined) {
                source.attr('src', $(this).attr('src'));
            } else {
                source.attr('src', $(this).attr('data-big'));
            }
            $('.enlargeImageModalCap').html($(this).attr('data-cap'));
            $('#enlargeImageModal').modal('show');
            source.attr('alt', $(this).attr('alt'));
        }
    });
});