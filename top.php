<?php
$phpSelf = htmlentities($_SERVER['PHP_SELF'], ENT_QUOTES, "UTF-8");
$fname = pathinfo($phpSelf)['filename'];
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <?php

    $a_icons = array("research", "brian-tivnan");
    if (in_array($fname, $a_icons)) {
        echo('<link rel="stylesheet" href="academicons/css/academicons.min.css"/>');
    }
    ?>

    <link href="css/custom.css" rel="stylesheet">

    <title>Computational Finance Lab</title>

    <?php
    if (file_exists ( 'owa/owa_php.php')) {
        require_once('owa/owa_php.php');
        $owa = new owa_php();
        // Set the site id you want to track
        $owa->setSiteId('8e83a78f04071d9a19dcc7c670aea49c');
        $owa->setPageTitle($fname);
        $owa->trackPageView();
    }
    ?>
</head>

<?php
//$phpSelf = htmlentities($_SERVER['PHP_SELF'], ENT_QUOTES, "UTF-8");
$fname = pathinfo($phpSelf)['filename'];

$isActive = function($page) use ($fname) {
    if ($fname == $page) {
        echo 'active';
    }
}
?>

<body id="<?php echo $fname; ?>">

    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php">Comp Fi Lab</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- todo replace with php function -->
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav active">
                <li class="nav-item <?php $isActive("index") ?>">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item <?php $isActive("about") ?>">
                    <a class="nav-link" href="about.php">About</a>
                </li>
                <li class="nav-item <?php $isActive("team") ?>">
                    <a class="nav-link" href="team.php">Team</a>
                </li>
                <li class="nav-item <?php $isActive("tools") ?>">
                    <a class="nav-link" href="visualizer.php">Visualizer</a>
                </li>
                <li class="nav-item <?php $isActive("research") ?>">
                    <a class="nav-link" href="research.php">Research</a>
                </li>
                <li class="nav-item <?php $isActive("talks") ?>">
                    <a class="nav-link" href="talks.php">Talks</a>
                </li>
                <li class="nav-item <?php $isActive("press") ?>">
                    <a class="nav-link" href="press.php">Press</a>
                </li>
            </ul>
        </div>
    </nav>
<div class="container">