<?php
include "top.php";
?>

<div class="jumbotron jumbotron-fluid rounded">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="img/profile/danforth.jpg" class="rounded mx-auto d-block w-100" alt="Chris Danforth">
            </div>

            <div class="col-md-6">
                <h1 class="display-4">Dr. Christopher Danforth</h1>
                <p class="lead">Associate Professor at UVM and Co-Director of Computational Story Lab.
                </p>
                <p>Danforth is an applied mathematician at the Vermont Complex Systems Center. His background is in the application of
                    chaos theory to weather & climate prediction, and his current work explores human behavior through the building of
                    instruments for computational social science. Danforth and his research partner Peter Sheridan Dodds are
                    co-directors of the Computational Story Lab, and co-inventors of
                    <a href="http://hedonometer.org,">hedonometer.org</a>, a socio-technical
                    instrument measuring daily happiness based on 100 billion Twitter messages. Danforth has also developed algorithms
                    to identify predictors of depression from Instagram photos. His research has been covered by the New York Times,
                    Science Magazine, and the BBC among others. Descriptions of his projects are available at his
                    <a href="http://uvm.edu/~cdanfort">website</a>.</p>
                <ul class="fa-ul">
                    <li class="list-item"><span class="fa fa-globe fa-2x">
                        <a class="unlink" href="http://uvm.edu/~cdanfort">Website</a>
                    </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<?php
include "footer-min.php";
?>