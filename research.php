<?php
include "top.php";
include "zoom.php";

$srep_cap = "<p>Ultra fast extreme events (UEEs). (A) Crash. Stock symbol is ABK. Date is 11/04/2009. Number of 
sequential down ticks is 20. Price change is 20.22. Duration is 25 ms (i.e. 0.025 seconds). The UEE duration is the time
 difference between the first and last tick in the sequence of jumps in a given direction. Percentage price change 
 downwards is 14% (i.e. crash size is 0.14 expressed as a fraction). (B) Spike. Stock symbol is SMCI. 
 Date is 10/01/2010. Number of sequential up ticks is 31. Price change is 12.75. Duration is 25 ms (i.e. 0.025 seconds). 
 Percentage price change upwards is 26% (i.e.spike size is 0.26 expressed as a fraction). Dots in price chart are sized 
 according to volume of trade. (C) Cumulative number of crashes (red) and spikes(blue) compared to overall stock market
 index (Standard & Poor’s 500) in black, showing daily close data from 3 Jan 2006 until 3 Feb 2011. Green horizontal 
 lines show periods of escalation of UEEs. Non-financials are dashed green horizontal lines, financials are solid green.
  20 most susceptible stock(i.e. most UEEs) are shown in ranked order from bottom to top, with Morgan Stanley (MS) 
  having the most UEEs.</p>";

$mk_cap = "<p>Examples of order book density plots. aOrder book from the Preis model, depicting homogeneous distribution
 of liquidity and prices over a limited range. bOrder book from our model with a market shock, where the price drops and
  recovers rather quickly, indicating that an influx of sell market orders occurred between time 520 and 620, and a 
  greater number of buy market orders occurred between time 620 and 700.cOrder book from our final model for an 
  experiment with σ=50. Note the two dark red buy orders near the spread at times 585 and 660. Since they are close to
   the spread, they are quickly filled by market orders. Contrast this with the dark red buy order placed further from
    the spread at time step 650, which remains unfilled in the order book for over 20 periods until eventually cancelled
     at time 674.</p>";

$fms_cap = "<p>A diagram of the plumbing of critical components of the financial system. As funding, collateral and 
securities flow through the system, they are not simply shuffled from one institution to another. The institutions take
the flows and transform them in various ways. The flows going from depositors to the long-term borrowers are subject to
 a maturity transformation, the standard banking function of taking in short term deposits and making longer-maturity
  loans.</p>";

$aapl_cap = "<p>Dislocation segments in AAPL across the 2016 calendar year are plotted in real-time, 
modulo day. Note the non-uniform density, where a large number of dislocation segments occur near the 
beginning of the trading day, around noon, and around 2pm. Additionally, these periods of instability also seem to host
 the largest magnitude opportunities, with open featuring a ~$0.35 potential arbitrage opportunity.</p>";

$gale_cap = "<p>Dislocation segments in GALE from July 2016 to June 2017 are plotted in real-time, 
modulo day. Note the non-uniform density, where a large number of dislocation segments occur near the 
beginning of the trading day.</p>"

?>

<div class="container mt-2">
    <div class="headline mb-3">
        <h2>Fragmentation and Inefficiencies in the U.S. Equity Markets: Evidence from the Dow 30</h2>
    </div>
    <p><small>February 2019 by Brian F. Tivnan, David Rushing Dewhurst, Colin M. Van Oort, John H. Ring IV, Tyler J. Gray,
            Brendan F. Tivnan, Matthew T. K. Koehler, Matthew T. McMahon, David Slater, Jason Veneman,
            Christopher M. Danforth</small></p>
    <div class="row">
        <figure class="col-md-4">
            <img src="img/abstracts/AAPL-400.png" data-big="img/abstracts/AAPL.png" data-cap="<?php echo($aapl_cap); ?>" class="img-fluid zoom" alt="AAPL LAO's">
            <figcaption>
                <p>Figure 8 from publication. Dislocation segments in AAPL across the 2016 calendar year,
                    click the image for more information.</p>
            </figcaption>
        </figure>
        <div class="col-md-8 float-left">
            <p>Using the most comprehensive source of commercially available data on the US National Market System, we
                analyze all quotes and trades associated with Dow 30 stocks in 2016 from the vantage point of a single
                and fixed frame of reference. Contrary to prevailing academic and popular opinion, we find that
                inefficiencies created in part by the fragmentation of the equity market place are widespread and
                potentially generate substantial profit for agents with superior market access. Information feeds
                reported different prices for the same equity, violating the commonly-supposed economic behavior of a
                unified price for an indistinguishable product more than 120 million times, with “actionable”
                dislocation segments totaling almost 64 million. During this period, roughly 22% of all trades
                occurred while the SIP and aggregated direct feeds were dislocated. The current market configuration
                resulted in a realized opportunity cost totaling over $160 million when compared with a single feed,
                single exchange alternative a conservative estimate that does not take into account intra-day
                offsetting events.</p>
            <ul class="ai-ul list-inline">
                <li class="list-inline-item">
                <span class="ai ai-arxiv-square ai-1x">
                    <a href="https://arxiv.org/abs/1902.04690">arXiv</a>
                </span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="headline mb-3">
        <h2>Scaling of inefficiencies in the U.S. equity markets: Evidence from three market indices and more than 2900 securities</h2>
    </div>
    <p><small>February 2019 by David Rushing Dewhurst, Colin M. Van Oort, John H. Ring IV, Tyler J. Gray, Christopher M. Danforth, Brian F. Tivnan </small></p>
    <div class="row">
        <figure class="col-md-4">
            <img src="img/abstracts/GALE-400.png" data-big="img/abstracts/GALE.png" data-cap="<?php echo($gale_cap); ?>" class="img-fluid zoom" alt="GALE LAO's">
            <figcaption>
                <p>Figure 3 from publication. Dislocation segments in GALE from July 2016 to June 2017,
                    click the image for more information. </p>
            </figcaption>
        </figure>
        <div class="col-md-8 float-left">
            <p>Using the most comprehensive, commercially-available dataset of trading activity in U.S. equity markets,
                we catalog and analyze dislocation segments and realized opportunity costs(ROC) incurred by
                market participants. We find that dislocation segments are common, observing a total of over
                3.1 billion dislocation segments in the Russell 3000 during trading in 2016, or roughly 525
                per second of trading. Up to 23% of observed trades may have contributed the the measured inefficiencies,
                leading to a ROC greater than $2 billion USD. A subset of the constituents of the S&P 500 index
                experience the greatest amount of ROC and may drive inefficiencies in other stocks. In addition, we
                identify fine structure and self-similarity in the intra-day distribution of dislocation segment start
                times. These results point to universal underlying market mechanisms arising from the
                physical structure of the U.S. National Market System.</p>
            <ul class="ai-ul list-inline">
                <li class="list-inline-item">
                <span class="ai ai-arxiv-square ai-1x">
                    <a href="https://arxiv.org/abs/1902.04691">arXiv</a>
                </span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="headline mb-3">
        <h2>An agent-based model for financial vulnerability</h2>
    </div>
    <p><small>July 2018 by Richard Bookstaber, Mark Paddrik, and Brian Tivnan.</small></p>
    <div class="row">
        <figure class="col-md-4">
            <img src="img/abstracts/financial_vulnerability_fig1-400.png" data-cap="<?php echo($fms_cap); ?>"
                 data-big="img/abstracts/financial_vulnerability_fig1.png" class="img-fluid zoom" alt="The plumbing of critical components of the financial system.">
            <figcaption>
                <p>Figure 2 from publication. The plumbing of critical components of the financial system. Click image
                for more information.</p>
            </figcaption>
        </figure>
        <div class="col-md-8 float-left">
            <p>This study addresses a critical regulatory shortfall by developing a platform to extend stress testing
                from a microprudential approach to a dynamic, macroprudential approach. This paper describes the ensuing
                agent-based model for analyzing the vulnerability of the financial system to asset- and funding-based
                fire sales. The model captures the dynamic interactions of agents in the financial system extending from
                the suppliers of funding through the intermediation and transformation functions of the bank/dealers to
                the financial institutions that use the funds to trade in the asset markets. The model replicates the
                key finding that it is the reaction to initial losses, rather than the losses themselves, that determine
                the extent of a crisis. By building on a detailed mapping of the transformations and dynamics of the
                financial system, the agent-based model provides an avenue toward risk management that can illuminate
                the pathways for the propagation of key crisis dynamics such as fire sales and funding runs.</p>
            <ul class="ai-ul list-inline">
                <li class="list-inline-item">
                    <span class="fa fa-file fa-1x">
                        <a href="https://www.financialresearch.gov/working-papers/files/OFRwp2014-05_BookstaberPaddrikTivnan_Agent-basedModelforFinancialVulnerability_revised.pdf">Working Paper</a>
                    </span>
                </li>
                <li class="list-inline-item">
                    <span class="ai ai-springer-square ai-1x">
                        <a href="https://link.springer.com/article/10.1007%2Fs11403-017-0188-1">Publication</a>
                    </span>
                </li>
                <li class="list-inline-item">
                    <span class="fa fa-file fa-1x">
                        <a href="https://www.financialresearch.gov/working-papers/files/OFRwp2014-05_BookstaberPaddrikTivnan_Agent-basedModelforFinancialVulnerability_revised_ATTACHMENT.pdf">Supplemental</a>
                    </span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="headline mb-3">
        <h2>Toward an understanding of market resilience: market liquidity and heterogeneity in the investor decision cycle</h2>
    </div>
    <p><small>October 2016 by Richard Bookstaber, Michael Foley, and Brian Tivnan.</small></p>
    <div class="row">
        <figure class="col-md-4">
            <img src="img/abstracts/market_liquidity_fig3b-400.png" data-big="img/abstracts/market_liquidity_fig3b.png"
                 data-cap="<?php echo($mk_cap); ?>" class="img-fluid zoom" alt="Order density plot.">
            <figcaption>
                <p>Panel b from figure 3 of publication. Order density plots (click image for more information).</p>
            </figcaption>
        </figure>
        <div class="col-md-8 float-left">
            <p>During liquidity shocks such as occur when margin calls force the liquidation of leveraged positions,
                there is a widening disparity between the reaction speed of the liquidity demanders and the
                liquidity providers. Those who are forced to sell typically must take action within the span of a
                day, while those who are providing liquidity do not face similar urgency. Indeed, the flurry of
                activity and increased volatility of prices during the liquidity shocks might actually reduce the
                speed with which many liquidity providers come to the market. To analyze these dynamics, we build
                upon previous agent-based models of financial markets, and specifically the Preis et. al (Europhys
                Lett 75(3):510–516, 2006) model, to develop an order-book model with heterogeneity in trader
                decision cycles. The model demonstrates an adherence to important stylized facts such as a
                leptokurtic distribution of returns, decay of autocorrelations over moderate to long time lags,
                and clustering volatility. Consistent with empirical analysis of recent market events, we
                demonstrate the impact of heterogeneous decision cycles on market resilience and the stochastic
                properties of market prices.</p>
            <ul class="ai-ul list-inline">
                <li class="list-inline-item">
                <span class="fa fa-file fa-1x">
                    <a href="https://www.financialresearch.gov/working-papers/files/OFRwp-2015-03_Market-Liquidity-and-Heterogeneity-in-Investor-Decision-Cycle.pdf">Working Paper</a>
                </span>
                </li>
                <li class="list-inline-item">
                <span class="ai ai-springer-square ai-1x">
                    <a href="https://link.springer.com/article/10.1007/s11403-015-0162-8">Publication</a>
                </span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="container">
    <div class="headline mb-3">
        <h2>Abrupt rise of new machine ecology beyond human response time</h2>
    </div>
    <p><small>September 11, 2013 by Neil Johnson, Guannan Zhao, Eric Hunsader, Hong Qi, Nicholas Johnson, Jing Meng, and Brian Tivnan.</small></p>
    <div class="row">
        <figure class="col-md-4">
            <img src="img/abstracts/srep_fig1-400.png" data-big="img/abstracts/srep_fig1.png"
                 data-cap="<?php echo($srep_cap); ?>" class="img-fluid zoom" alt="Ultrafast Extreme Events">
            <figcaption>
                <p>Figure 1 in publication. A depiction of Ultrafast Extreme Events (UEEs), click image for more details.</p>
            </figcaption>
        </figure>
        <div class="col-md-8 float-left">
            <p>Society's techno-social systems are becoming ever faster and more computer-orientated. However, far from
                simply generating faster versions of existing behaviour, we show that this speed-up can generate a new
                behavioural regime as humans lose the ability to intervene in real time. Analyzing millisecond-scale
                data for the world's largest and most powerful techno-social system, the global financial market, we
                uncover an abrupt transition to a new all-machine phase characterized by large numbers of subsecond
                extreme events. The proliferation of these subsecond events shows an intriguing correlation with the
                onset of the system-wide financial collapse in 2008. Our findings are consistent with an emerging
                ecology of competitive machines featuring ‘crowds’ of predatory algorithms, and highlight the need for
                a new scientific theory of subsecond financial phenomena.</p>
            <ul class="ai-ul list-inline">
                <li class="list-inline-item">
                <span class="ai ai-arxiv-square ai-1x">
                    <a href="https://arxiv.org/abs/1202.1448">arXiv</a>
                </span>
                </li>
                <li class="list-inline-item">
                <span class="ai ai-springer-square ai-1x">
                    <a href="https://www.nature.com/articles/srep02627?d=09-22-2013">Publication</a>
                </span>
                </li>
                <li class="list-inline-item">
                <span class="fa fa-file fa-1x">
                    <a href="https://media.nature.com/original/nature-assets/srep/2013/130911/srep02627/extref/srep02627-s1.pdf">Supplemental</a>
                </span>
                </li>
            </ul>
        </div>
    </div>
</div><!--/container-->

<?php
include "footer.php";
?>