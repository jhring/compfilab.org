<div class="modal fade" id="enlargeImageModal" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <img src="#" class="enlargeImageModalSource w-100" alt="">
            </div>
            <div class="text-center text-dark"><p class="enlargeImageModalCap"></p></div>
        </div>
    </div>
</div>
