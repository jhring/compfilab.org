<?php
include "top.php";
?>

<div class="container">
    <div class="headline mb-3">
        <h2><a class="unlink" href="https://www.wsj.com/articles/brief-price-gaps-in-stocks-cost-investors-2-billion-a-year-11550152800">Brief Price Gaps in Stocks Cost Investors $2 Billion a Year</a></h2>
    </div>
    <p><small>Febuary 2019, by The Wall Street Journal</small></p>
    <div class="row">
        <figure class="col-md-4">
            <a href="https://www.wsj.com/articles/brief-price-gaps-in-stocks-cost-investors-2-billion-a-year-11550152800">
                <img src="https://images.wsj.net/im-53811?width=1260&aspect_ratio=1.5" alt="The New York Stock Exchange" class="img-fluid">
            </a>
        </figure>
        <div class="col-md-4 float-left">
            <p>A federally funded study provides new evidence of momentary pricing discrepancies that researchers say
                can be exploited by high-speed traders looking to make a quick profit.</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="headline mb-3">
        <h2><a class="unlink" href="https://www.wsj.com/articles/a-new-way-to-spot-the-next-financial-crisis-11545131907">A New Way to Spot the Next Financial Crisis</a></h2>
    </div>
    <p><small>December 2018, by The Wall Street Journal</small></p>
    <div class="row">
        <figure class="col-md-4">
            <a href="https://www.wsj.com/articles/a-new-way-to-spot-the-next-financial-crisis-11545131907">
                <img src="https://images.wsj.net/im-43149" alt="The U.S. Treasury Department building in Washington, DC." class="img-fluid">
            </a>
        </figure>
        <div class="col-md-4 float-left">
            <p>A more realistic way to model financial risk is emerging. It could help big banks and regulators
                spot potholes, even if it can’t stop people falling into them.</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="headline mb-3">
        <h2><a class="unlink" href="https://nyti.ms/VVo7vc">Clouds Seen in Regulators’ Crystal Ball for Banks</a></h2>
    </div>
    <p><small>January 2013, by The New York Times</small></p>
    <div class="row">
        <figure class="col-md-4">
        <a href="https://nyti.ms/VVo7vc">
            <img src="https://static01.nyt.com/newsgraphics/images/icons/defaultPromoCrop.png" alt="NYT Logo" class="img-fluid">
        </a>
        </figure>
        <div class="col-md-4 float-left">
            <p>The models used to assess financial risk are still flawed, according to a new report from a Treasury Department agency, the Office of Financial Research.</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="headline mb-3">
        <h2><a class="unlink" href="https://www.wired.com/2012/02/high-speed-trading/">Nanosecond Trading Could Make Markets Go Haywire</a></h2>
    </div>
    <p><small>Febuary 2012, by Wired</small></p>
    <div class="row">
        <figure class="col-md-4">
            <a href="https://www.wired.com/2012/02/high-speed-trading/">
                <img src="https://media.wired.com/photos/5933296d68cb3b3dc409807e/master/w_660,c_limit/stockprices.jpg" alt="High Speed Trading" class="img-fluid">
            </a>
        </figure>
        <div class="col-md-4 float-left">
            <p>The afternoon of May 6, 2010 was among the strangest in economic history. Starting at 2:42 p.m. EDT, the
                Dow Jones stock index fell 600 points in just 6 minutes. Its nadir represented the deepest single-day
                decline in that market’s 114-year history. By 3:07 p.m., the index had rebounded. The “flash crash,”
                as it came ...</p>
        </div>
    </div>
</div>

<?php
include "footer.php";
?>
