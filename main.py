from os import listdir
from os.path import isfile, join

files = [f for f in listdir("img/circle_plots/") if isfile(join("img/circle_plots/", f))]
final = []

tickers = [x.split('_')[0] for x in files]

for ticker in tickers:
    if tickers.count(ticker) >= 4:
        final.append(ticker)
    else:
        print(ticker)

final = sorted(set(final))


#files = [f for f in listdir("img/circle_plots/log_scaled/png") if isfile(join("img/circle_plots/log_scaled/png", f))]
#tickers2 = sorted(set([x.split('_')[0] for x in files]))
print(final)
#print(tickers == tickers2)
