<?php
include "top.php";
?>

<div class="jumbotron jumbotron-fluid rounded">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="img/profile/koehler.jpg" class="rounded mx-auto d-block w-100" alt="Koehler">
            </div>

            <div class="col-md-6">
                <h1 class="display-4">Dr. Matt Koehler</h1>
                <p class="lead">Complexity Science Area Lead at The MITRE Corporation
                </p>
                <p>Matt Koehler joined MITRE in 2004 after being a Presidential Management Fellow with the Center for
                    Army Analysis.  Currently Matt is the chief computational social scientist for MITRE’s Modeling,
                    Simulation, Experimentation, and Analytics Technical Center where he grows, coordinates, and
                    technically contributes to MITRE computational social science work.  In addition, he is MITRE’s
                    liaison to the Santa Fe Institute.  Matt holds degrees in Anthropology, Public Affairs, Law, and
                    Computational Social Science.</p>
            </div>
        </div>
    </div>
</div>


<?php
include "footer-min.php";
?>
