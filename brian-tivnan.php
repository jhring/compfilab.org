<?php
include "top.php";
?>

<div class="jumbotron jumbotron-fluid rounded">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="img/profile/btivnan.jpg" class="rounded mx-auto d-block w-100" alt="Brian Tivnan">
            </div>

            <div class="col-md-6">
                <h1 class="display-4">Dr. Brian Tivnan</h1>
                <p class="lead">Chief Engineer at The MITRE Corporation and an Adjunct Assistant Professor
                    in the Department of Mathematics and Statistics at the University of Vermont.
                </p>
                <p>Brian joined UVM’s Complex Systems Center in 2010 as a founding member of the Center’s Steering Committee.
                    He is the site leader for MITRE-Vermont and Chief Engineer for the Modeling & Simulation division
                    of The MITRE Corporation.  Because of large-scale complex problems, MITRE and its partners are tackling
                    and solving problems for a safer world.  Along with Peter Dodds and Chris Danforth, Brian co-developed
                    a real-time, remote sensor of global happiness using messages from Twitter and other media: the Hedonometer;
                    and he also launched the collaborative research program in Computational Finance between MITRE and UVM known
                    as the. ComputationalFinanceLab</p>
                <ul class="ai-ul">
                    <li class="list-item"><span class="ai ai-google-scholar-square ai-3x">
                            <a href="https://scholar.google.com/citations?user=RAkbCj0AAAAJ&hl=en&oi=ao">link</a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<?php
include "footer-min.php";
?>
