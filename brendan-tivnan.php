<?php
include "top.php";
?>

<div class="jumbotron jumbotron-fluid rounded">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="img/profile/brendan.jpg" class="rounded mx-auto d-block w-100" alt="Brendan Tivnan">
            </div>

            <div class="col-md-6">
                <h1 class="display-4">Brendan Tivnan</h1>
                <p class="lead">
                    Research Intern at UVM's Complex Systems Center
                </p>
                <p>Brendan joined UVM’s Complex Systems Center in 2016 as a Research Intern.  His research focus has been on
                    Computational Finance, specifically the measurement and modeling of market stability.
                    He is a member of the Computational Finance Lab.  </p>
            </div>
        </div>
    </div>
</div>


<?php
include "footer-min.php";
?>
